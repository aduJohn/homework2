function addTokens(input, tokens){
    if(Object.prototype.toString.call(input) === "[object String]"){
        if(input.length>5){
            tokens.forEach(element => {
                Object.entries(element).forEach(([key,value])=>{
                    if(key!=="tokenName"||Object.prototype.toString.call(value)!=="[object String]"){
                        throw new Error("Invalid array format")
                    }
                });
            });
            if(!input.includes("...")){
                return input
            }
            tokens.forEach(element=>{
                Object.entries(element).forEach(([key,value])=>{
                    input = input.replace("...","${"+value+"}")
                });
            });
            return input
        }
        else{
            throw new Error("Input should have at least 6 characters")
        }
    }else{
        throw new Error("Invalid input")
    }
}

const app = {
    addTokens: addTokens
}

module.exports = app;